import firestore from '@react-native-firebase/firestore';

import {sortBy} from 'lodash';

const ref = firestore().collection('albums');

export const getAlbums = async () => {
  const querySnapshot = await ref.get();
  const albums = [];
  querySnapshot.forEach((doc) => {
    albums.push({
      id: doc.id,
      ...doc.data(),
    });
  });

  const listAlbums = sortBy(albums, 'name');

  return listAlbums;
};

export const getAlbumDetails = async (category) => {
  const ref2 = firestore()
    .collection('songs')
    .where('category', 'array-contains', category);
  const querySnapshot = await ref2.get();
  const album = [];
  querySnapshot.forEach((doc) => {
    album.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
    });
  });

  const listAlbum = sortBy(album, 'name');

  return listAlbum;
};
