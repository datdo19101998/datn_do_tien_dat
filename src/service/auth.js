import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';

import {sortBy} from 'lodash';

export const postUser = async () => {
  try {
    const userId = auth().currentUser.uid;
    const getUser = auth().currentUser;
    const providerData = auth().currentUser.providerData[0];
    await firestore()
      .collection('users')
      .doc(userId)
      .collection('infomation')
      .doc(userId)
      .set({
        displayName: providerData.displayName,
        email: providerData.email,
        isAnonymous: getUser.isAnonymous,
        metadata: {
          creationTime: new Date(getUser.metadata.creationTime).toString(),
          lastSignInTime: new Date(getUser.metadata.lastSignInTime).toString(),
        },
        phoneNumber: providerData.phoneNumber,
        photoURL: providerData.photoURL,
        providerData: [
          {
            providerId: providerData.providerId,
            uid: providerData.uid,
          },
        ],
        providerId: providerData.providerId,
        uid: getUser.uid,
      });
    return 'created an user';
  } catch (e) {
    return e.message;
  }
};

export const getCurrentUser = async () => {
  var data = null;
  await auth().onAuthStateChanged((userChange) => {
    data = userChange;
  });
  return data;
};

export const getRecent = async (id) => {
  const ref3 = firestore()
    .collection('users')
    .doc(`${id}`)
    .collection('recents');
  const querySnapshot = await ref3.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push(doc.data());
  });
  const listRecent = sortBy(songs, 'name');
  return listRecent;
};
