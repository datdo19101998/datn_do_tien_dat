import firestore from '@react-native-firebase/firestore';

import {sortBy} from 'lodash';

const ref3 = firestore().collection('singers');

export const getSingers = async () => {
  const querySnapshot = await ref3.get();
  const singers = [];
  querySnapshot.forEach((doc) => {
    singers.push({
      id: doc.id,
      ...doc.data(),
    });
  });

  const listSingers = sortBy(singers, 'hearts').reverse();
  return listSingers;
};

export const getSingerDetail = async (category) => {
  const ref = firestore()
    .collection('songs')
    .where('category', 'array-contains', category);
  const querySnapshot = await ref.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
    });
  });

  const listSongs = sortBy(songs, 'name');

  return listSongs;
};
