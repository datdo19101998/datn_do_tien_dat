import firestore from '@react-native-firebase/firestore';
import {sortBy, findIndex} from 'lodash';

const ref3 = firestore().collection('songs');

const ref2 = firestore().collection('plays');

export const getSong = async () => {
  const querySnapshot = await ref3.get();
  const songs = [];
  querySnapshot.forEach((doc) => {
    songs.push({
      id: doc.id,
      title: doc.data().name,
      artist: doc.data().singer,
      artwork: doc.data().url.cover,
      url: doc.data().url.play,
      likes: doc.data().likes,
      plays: doc.data().plays,
    });
  });
  const listSongs = sortBy(songs, 'plays', 'desc').reverse();
  return listSongs;
};

export const addSongToDate = async (data) => {
  let querySnapshot = await ref2.get();
  const items = [];
  querySnapshot.forEach((doc) => {
    items.push(doc.data());
  });

  const index = findIndex(items, {title: data.title});
  var item = {};
  await ref2
    .where('id', '==', data.id)
    .get()
    .then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        item = {...doc.data(), id: doc.id};
      });
    })
    .catch(function (error) {
      console.log('Error getting documents: ', error);
    });
  const {price, id, todayDate} = item;

  if (index === -1 && todayDate !== data.todayDate) {
    firestore()
      .collection('plays')
      .add({...data, price: 1})
      .then(() => {
        console.log(`added song ++++ ${data.title} ++++ to plays`);
      });
  } else {
    ref2
      .doc(id)
      .update({
        price: price + 1,
      })
      .then(() => {
        console.log('update price');
      });
  }
  return data;
};
