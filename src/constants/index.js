export const GET_SONG_REQUEST = 'GET_SONG_REQUEST';

export const GET_SONG_SUCCESS = 'GET_SONG_SUCCESS';

export const GET_SONG_ERROR = 'GET_SONG_ERROR';

export const GET_SINGER_REQUEST = 'GET_SINGER_REQUEST';

export const GET_SINGER_SUCCESS = 'GET_SINGER_SUCCESS';

export const GET_SINGER_ERROR = 'GET_SINGER_ERROR';

export const GET_ALBUM_REQUEST = 'GET_ALBUM_REQUEST';

export const GET_ALBUM_SUCCESS = 'GET_ALBUM_SUCCESS';

export const GET_ALBUM_ERROR = 'GET_ALBUM_ERROR';

export const GET_ALBUM_DETAIL_REQUEST = 'GET_ALBUM_DETAIL_REQUEST';

export const GET_ALBUM_DETAIL_SUCCESS = 'GET_ALBUM_DETAIL_SUCCESS';

export const GET_ALBUM_DETAIL_ERROR = 'GET_ALBUM_DETAIL_ERROR';

export const GET_SINGER_DETAIL_REQUEST = 'GET_SINGER_DETAIL_REQUEST';

export const GET_SINGER_DETAIL_SUCCESS = 'GET_SINGER_DETAIL_SUCCESS';

export const GET_SINGER_DETAIL_ERROR = 'GET_SINGER_DETAIL_ERROR';

export const GET_SONG_PLAYING = 'GET_SONG_PLAYING';

export const GET_SONG_PLAYING_SUCCESS = 'GET_SONG_PLAY_SUCCESS';

export const POST_USER_REQUEST = 'POST_USER_REQUEST';

export const POST_USER_SUCCESS = 'POST_USER_SUCCESS';

export const POST_USER_ERROR = 'POST_USER_ERROR';

export const GET_CURRENT_USER_REQUEST = 'GET_CURRENT_USER_REQUEST';

export const GET_CURRENT_USER_SUCCESS = 'GET_CURRENT_USER_SUCCESS';

export const GET_CURRENT_USER_ERROR = 'GET_CURRENT_USER_ERROR';

export const GET_RECENT_REQUEST = 'GET_RECENT_REQUEST';

export const GET_RECENT_SUCCESS = 'GET_RECENT_SUCCESS';

export const GET_RECENT_ERROR = 'GET_RECENT_ERROR';
