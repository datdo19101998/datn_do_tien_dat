import * as types from '../../constants';

export const getAlbums = () => ({
  type: types.GET_ALBUM_REQUEST,
});

export const getAlbumDetails = (payload) => ({
  type: types.GET_ALBUM_DETAIL_REQUEST,
  payload,
});
