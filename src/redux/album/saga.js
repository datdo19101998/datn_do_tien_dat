import * as types from '../../constants/index';

import * as Service from '../../service/album';

import {takeEvery, all, call, put, fork} from 'redux-saga/effects';

export function* getAlbums() {
  yield takeEvery(types.GET_ALBUM_REQUEST, function* () {
    try {
      const resp = yield call(Service.getAlbums);
      yield put({
        type: types.GET_ALBUM_SUCCESS,
        albums: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_ALBUM_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getAlbumDetail() {
  yield takeEvery(types.GET_ALBUM_DETAIL_REQUEST, function* ({payload}) {
    const {category} = payload;
    try {
      const resp = yield call(Service.getAlbumDetails, category);

      yield put({
        type: types.GET_ALBUM_DETAIL_SUCCESS,
        album: resp,
        name: category,
      });
    } catch (err) {
      yield put({
        type: types.GET_ALBUM_DETAIL_ERROR,
        error: err.message,
      });
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getAlbums)]);
  yield all([fork(getAlbumDetail)]);
}
