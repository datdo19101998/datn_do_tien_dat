import * as types from '../../constants/index';

const initialState = {
  albums: [],
  album: [],
  name: '',
  error: null,
};

const reducer = (state = initialState, {type, album, albums, error, name}) => {
  switch (type) {
    case types.GET_ALBUM_SUCCESS:
      return {...state, albums: albums};
    case types.GET_ALBUM_ERROR:
      return {...state, error: error};
    case types.GET_ALBUM_DETAIL_SUCCESS:
      return {...state, album: album, name: name};
    case types.GET_ALBUM_DETAIL_ERROR:
      return {...state, error: error};
    default:
      return {...state};
  }
};

export default reducer;
