import * as types from '../../constants';

export const postUser = () => ({
  type: types.POST_USER_REQUEST,
});

export const getCurrentUser = () => ({
  type: types.GET_CURRENT_USER_REQUEST,
});

export const getRecent = (payload) => ({
  type: types.GET_RECENT_REQUEST,
  payload,
});
