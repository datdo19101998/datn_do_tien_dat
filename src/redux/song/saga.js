import * as types from '../../constants';
import {all, takeEvery, put, fork, call} from 'redux-saga/effects';

import moment from 'moment';

import * as Service from '../../service/song';

export function* getSong() {
  yield takeEvery(types.GET_SONG_REQUEST, function* () {
    try {
      const resp = yield call(Service.getSong);
      yield put({
        type: types.GET_SONG_SUCCESS,
        songs: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_SONG_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getSongPlaying() {
  let todayDate = moment().format('YYYY/MM/DD');

  yield takeEvery(types.GET_SONG_PLAYING, function* ({payload}) {
    // console.log('payload', payload);
    const resp = yield call(Service.addSongToDate, {...payload, todayDate});
    yield put({
      type: types.GET_SONG_PLAYING_SUCCESS,
      song: resp,
    });
  });
}

export default function* rootSaga() {
  yield all([fork(getSong)]);
  yield all([fork(getSongPlaying)]);
}
