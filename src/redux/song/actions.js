import * as songConstant from '../../constants';

export const getSong = () => ({
  type: songConstant.GET_SONG_REQUEST,
});

export const getSongIsPlaying = (payload) => ({
  type: songConstant.GET_SONG_PLAYING,
  payload,
});

export const getSongPlaying = (payload) => ({
  type: songConstant.GET_SONG_PLAYING,
  payload,
});
