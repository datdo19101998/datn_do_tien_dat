import * as types from '../../constants/index';

const initialState = {
  songs: [],
  song: {},
  error: null,
};
const reducer = (state = initialState, {type, songs, song, error}) => {
  switch (type) {
    case types.GET_SONG_SUCCESS:
      return {
        ...state,
        songs: songs,
      };
    case types.GET_SONG_ERROR:
      return {
        ...state,
        error: error,
      };
    case types.GET_SONG_PLAYING_SUCCESS:
      return {...state, song: song};
    default:
      return {...state};
  }
};
export default reducer;
