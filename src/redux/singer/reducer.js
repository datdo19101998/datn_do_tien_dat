import * as types from '../../constants/index';

const initialState = {
  singers: [],
  singer: [],
  informations: {},
  error: null,
};

const reducer = (
  state = initialState,
  {type, singers, singer, error, informations},
) => {
  switch (type) {
    case types.GET_SINGER_SUCCESS:
      return {...state, singers: singers};
    case types.GET_SINGER_ERROR:
      return {...state, error: error};
    case types.GET_SINGER_DETAIL_SUCCESS:
      return {...state, singer: singer, informations: informations};
    case types.GET_SINGER_DETAIL_ERROR:
      return {...state, error: error};
    default:
      return {...state};
  }
};

export default reducer;
