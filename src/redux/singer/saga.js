import * as types from '../../constants/index';

import {all, takeEvery, put, fork, call} from 'redux-saga/effects';

import * as Service from '../../service/singer';

export function* getSinger() {
  yield takeEvery(types.GET_SINGER_REQUEST, function* () {
    try {
      const resp = yield call(Service.getSingers);

      yield put({
        type: types.GET_SINGER_SUCCESS,
        singers: resp,
      });
    } catch (err) {
      yield put({
        type: types.GET_SINGER_ERROR,
        error: err.message,
      });
    }
  });
}

export function* getSingerDetail() {
  yield takeEvery(types.GET_SINGER_DETAIL_REQUEST, function* ({payload}) {
    const {category} = payload;
    try {
      const resp = yield call(Service.getSingerDetail, category);
      yield put({
        type: types.GET_SINGER_DETAIL_SUCCESS,
        singer: resp,
        informations: payload,
      });
    } catch (err) {
      yield put({type: types.GET_SINGER_DETAIL_ERROR, error: err.message});
    }
  });
}

export default function* rootSaga() {
  yield all([fork(getSinger)]);
  yield all([fork(getSingerDetail)]);
}
