import * as types from '../../constants';

export const getSinger = () => ({
  type: types.GET_SINGER_REQUEST,
});

export const getSingerDetail = (payload) => ({
  type: types.GET_SINGER_DETAIL_REQUEST,
  payload,
});
