import {all} from 'redux-saga/effects';
import Song from './song/saga';
import Singer from './singer/saga';
import Album from './album/saga';
import Auth from './auth/saga';

export default function* rootSaga() {
  yield all([Song(), Singer(), Album(), Auth()]);
}
