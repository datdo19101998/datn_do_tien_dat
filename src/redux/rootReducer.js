import {combineReducers} from 'redux';
import Songs from './song/reducer';
import Singer from './singer/reducer';
import Album from './album/reducer';
import Auth from './auth/reducer';
const rootReducer = combineReducers({
  Songs,
  Singer,
  Album,
  Auth,
});

export default rootReducer;
