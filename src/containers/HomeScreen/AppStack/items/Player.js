import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import TrackPlayer, {
  useTrackPlayerProgress,
  usePlaybackState,
  useTrackPlayerEvents,
} from 'react-native-track-player';
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewPropTypes,
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';

import * as SongAction from '../../../../redux/song/actions';

import Slider from '@react-native-community/slider';

import Moment from 'moment';

import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

function secondsToHms(d) {
  const date = Moment.utc(d * 1000).format('m:ss');
  return <Text style={[styles.textLight, styles.timeStamp]}>{date}</Text>;
}

function ControlButton({title, onPress}) {
  return (
    <TouchableOpacity style={styles.controlButtonContainer} onPress={onPress}>
      <Text style={styles.controlButtonText}>{title}</Text>
    </TouchableOpacity>
  );
}

ControlButton.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default function Player(props) {
  const playbackState = usePlaybackState();
  const progress = useTrackPlayerProgress();
  const dispatch = useDispatch();

  const {duration, position} = progress;

  const [trackTitle, setTrackTitle] = useState('');
  const [trackArtwork, setTrackArtwork] = useState();
  const [trackArtist, setTrackArtist] = useState('');

  const {song} = useSelector((state) => state.Songs);

  useEffect(() => {
    const {title, artist, artwork} = song;
    setTrackTitle(title);
    setTrackArtist(artist);
    setTrackArtwork(artwork);
  }, [song]);

  useTrackPlayerEvents(['playback-track-changed'], async (event) => {
    if (event.type === TrackPlayer.TrackPlayerEvents.PLAYBACK_TRACK_CHANGED) {
      const track = await TrackPlayer.getTrack(event.nextTrack);
      // if (track) {
      //   dispatch(SongAction.getSongPlaying(track));
      // }
      // console.log('track',track);
      const {title, artist, artwork} = track || {};
      setTrackTitle(title);
      setTrackArtist(artist);
      setTrackArtwork(artwork);
    }
  });

  const {style, onNext, onPrevious, onTogglePlayback, stateName} = props;

  var middleButtonText = (
    <AntDesign name={'playcircleo'} color={'white'} size={70} />
  );

  if (
    playbackState === TrackPlayer.STATE_PLAYING ||
    playbackState === TrackPlayer.STATE_BUFFERING
  ) {
    middleButtonText = (
      <AntDesign name={'pausecircleo'} color={'white'} size={70} />
    );
  }

  const changeTime = (seconds) => {
    TrackPlayer.seekTo(seconds);
  };

  return (
    <ImageBackground
      source={require('../../../../../images/hinh-nen-dep-cho-dep-thoai.jpg')}
      style={styles.background}>
      <View style={[styles.card, style]}>
        <View style={styles.imageContainer}>
          <Image style={styles.cover} source={{uri: trackArtwork}} />
        </View>
        <View>
          <Text style={styles.text}>{stateName}</Text>
        </View>
        <View style={styles.m20}>
          <Slider
            minimumValue={0}
            maximumValue={duration}
            trackStyle={style.track}
            minimumTrackTintColor={'orange'}
            maximumTrackTintColor={'white'}
            thumbTintColor={'orange'}
            value={position}
            onValueChange={(seconds) => changeTime(seconds)}
          />
          <View style={styles.duration}>
            {secondsToHms(position)}
            <Text style={[styles.textLight, style.timeStamp]}>
              {secondsToHms(duration)}
            </Text>
          </View>
        </View>
        <Text style={styles.title}>{trackTitle}</Text>
        <Text style={styles.artist}>{trackArtist}</Text>
        <View style={styles.controls}>
          <ControlButton
            title={<AntDesign name={'like2'} color={'white'} size={25} />}
          />
          <ControlButton
            title={<AntDesign name={'banckward'} color={'white'} size={40} />}
            onPress={onPrevious}
          />
          <ControlButton title={middleButtonText} onPress={onTogglePlayback} />
          <ControlButton
            title={<AntDesign name={'forward'} color={'white'} size={40} />}
            onPress={onNext}
          />
          <ControlButton
            title={
              <MaterialCommunityIcons
                name={'repeat-once'}
                color={'white'}
                size={25}
              />
            }
          />
        </View>
      </View>
    </ImageBackground>
  );
}

Player.propTypes = {
  style: ViewPropTypes.style,
  onNext: PropTypes.func.isRequired,
  onPrevious: PropTypes.func.isRequired,
  onTogglePlayback: PropTypes.func.isRequired,
};

Player.defaultProps = {
  style: {},
};

const styles = StyleSheet.create({
  card: {
    width: '95%',
    height: '95%',
  },
  cover: {
    width: 300,
    height: 300,
    marginTop: 20,
    backgroundColor: 'grey',
    borderRadius: 300,
    marginBottom: 70,
  },
  progress: {
    height: 3,
    width: '90%',
    marginTop: 10,
    flexDirection: 'row',
  },
  title: {
    marginTop: 10,
    color: 'white',
    textAlign: 'center',
  },
  artist: {
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
  },
  controls: {
    marginVertical: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  controlButtonContainer: {
    flex: 1,
  },
  controlButtonText: {
    fontSize: 18,
    textAlign: 'center',
  },
  text: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  track: {
    height: 2,
    borderRadius: 1,
    backgroundColor: '#FFF',
  },
  textLight: {
    color: 'white',
  },
  background: {
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  m20: {
    marginHorizontal: 32,
  },
  duration: {
    marginTop: -5,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
