import React, {useEffect, useState} from 'react';
import {SafeAreaView, StyleSheet, ActivityIndicator, View} from 'react-native';
import {useSelector} from 'react-redux';

import TrackPlayer, {usePlaybackState} from 'react-native-track-player';

import {isEmpty} from 'lodash';

import PlayerItem from './items/Player';

export default function Player({route}) {
  console.disableYellowBox = true;

  const playbackState = usePlaybackState();

  const [item, setItem] = useState({});
  const [items, setItems] = useState([]);

  const {songs, song} = useSelector((state) => state.Songs);

  useEffect(() => {
    setItems(songs);
    setItem(song);
  }, [song, songs]);

  const newData = [{...item}];
  const data = [...newData, ...items];

  useEffect(() => {
    setup();
    TrackPlayer.play();
  }, []);

  const setup = async () => {
    await TrackPlayer.setupPlayer({});
    await TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
        TrackPlayer.CAPABILITY_STOP,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
      ],
    });
  };

  async function togglePlayback() {
    if (!isEmpty(songs)) {
      const currentTrack = await TrackPlayer.getCurrentTrack();
      if (currentTrack == null) {
        await TrackPlayer.reset();

        await TrackPlayer.add(data);
        await TrackPlayer.play();
      } else {
        if (playbackState === TrackPlayer.STATE_PAUSED) {
          await TrackPlayer.play();
        } else {
          await TrackPlayer.pause();
        }
      }
    }
  }

  if (isEmpty(items)) {
    return (
      <View style={style.loading}>
        <ActivityIndicator size={'large'} />
      </View>
    );
  } else {
    return (
      <SafeAreaView style={style.container}>
        <PlayerItem
          onNext={skipToNext}
          onPrevious={skipToPrevious}
          onTogglePlayback={togglePlayback}
          stateName={getStateName(playbackState)}
        />
      </SafeAreaView>
    );
  }
}
const style = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    justifyContent: 'center',
  },
  loading: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function getStateName(state) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return 'None';
    case TrackPlayer.STATE_PLAYING:
      return 'Playing';
    case TrackPlayer.STATE_PAUSED:
      return 'Paused';
    case TrackPlayer.STATE_STOPPED:
      return 'Stopped';
    case TrackPlayer.STATE_BUFFERING:
      return 'Buffering';
  }
}

async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}
