import React, {useEffect, useState} from 'react';

//import properties UI of react native
import {
  View,
  StyleSheet,
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';

//import firebase library

import {useSelector, useDispatch} from 'react-redux';

//import action of redux
import * as SongAction from '../../../../redux/song/actions';

//import react native elements library
import {ListItem} from 'react-native-elements';

export default function HomeAlbum({route, navigation}, props) {
  console.disableYellowBox = true;

  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);

  const {album} = useSelector((state) => state.Album);

  useEffect(() => {
    if (album) {
      setLoading(false);
    }
  }, [album]);

  const goPlayPage = (item) => {
    dispatch(SongAction.getSongPlaying(item));
    navigation.navigate('Player');
  };

  // useEffect(() => {
  //   firestore()
  //     .collection('songs')
  //     .add({
  //       category: ['erik'],
  //       likes: 0,
  //       name: 'Em không sai chúng ta ',
  //       plays: 0,
  //       singer: '',
  //       url: {
  //         cover:
  //           'https://dgpiano.vn/wp-content/uploads/2020/05/740d5e0fd272d2421d441e9fd5c08fdd.jpg',
  //         play:
  //           'https://firebasestorage.googleapis.com/v0/b/rnlogin-b2b29.appspot.com/o/images%2FEm-Khong-Sai-Chung-Ta-Sai-ERIK.mp3?alt=media&token=c0ccb2b4-9ed4-4255-91cb-a0203ec12436',
  //       },
  //     })
  //     .then(() => console.log('created'));
  // }, []);

  //render react native app
  if (loading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator color={'green'} size={'large'} />
      </View>
    );
  }
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.pd10}>
        <FlatList
          data={album}
          renderItem={({item, index}) => (
            <TouchableOpacity onPress={() => goPlayPage(item)} key={item.index}>
              <ListItem
                leftAvatar={{source: {uri: item.artwork}}}
                title={item.title}
                titleStyle={styles.colorBlack}
                subtitle={item.artist}
                style={styles.mt5}
              />
            </TouchableOpacity>
          )}
          keyExtractor={(item) => item.id}
        />
      </View>
    </SafeAreaView>
  );
}

//styles for component
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  pd10: {
    padding: 10,
  },
  mt5: {
    marginTop: 5,
    borderRadius: 10,
  },
  colorBlack: {
    color: 'black',
  },
});
