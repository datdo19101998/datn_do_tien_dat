import React, {useEffect, useState} from 'react';

//import properties UI of react native
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  FlatList,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import {isEmpty} from 'lodash';

//import react-native-vector-icons
import Icon from 'react-native-vector-icons/FontAwesome5';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';

//import for redux
import {useSelector, useDispatch} from 'react-redux';

//import action of redux
import * as SongAction from '../../../../redux/song/actions';
import * as SingerAction from '../../../../redux/singer/actions';
import * as AlbumAction from '../../../../redux/album/actions';
import * as AuthAction from '../../../../redux/auth/actions';

import CurrentPlayer from '../CurrentPlayer';

export default function Home({navigation}, props) {
  console.disableYellowBox = true;
  const dispatch = useDispatch();

  //initial value state
  const [loading, setLoading] = useState(true);

  //value get of redux
  const {songs, song} = useSelector((state) => state.Songs);
  const {singers} = useSelector((state) => state.Singer);
  const {albums} = useSelector((state) => state.Album);

  // dispatch actions of redux is get value
  useEffect(() => {
    dispatch(SongAction.getSong());
    dispatch(SingerAction.getSinger());
    dispatch(AuthAction.postUser());
    dispatch(AlbumAction.getAlbums());
  }, [dispatch]);

  //get value of redux and setState
  useEffect(() => {
    setLoading(false);
  }, [albums, singers, songs]);

  //formatted number is more than 999
  const kFormatter = (num) => {
    return Math.abs(num) > 999
      ? Math.sign(num) * (Math.abs(num) / 1000).toFixed(1) + 'k'
      : Math.sign(num) * Math.abs(num);
  };

  const goSingerPage = (item) => {
    dispatch(SingerAction.getSingerDetail(item));
    navigation.navigate('HomeSinger');
  };

  const goPlayPage = (item) => {
    dispatch(SongAction.getSongPlaying(item));
    navigation.navigate('Player');
  };

  const goToAlbumsPage = (item) => {
    dispatch(AlbumAction.getAlbumDetails(item));
    navigation.navigate('HomeAlbum');
  };

  //render react native component
  if (loading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator color={'green'} size={'large'} />
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <SafeAreaView>
          <ScrollView style={{height: '100%'}}>
            <View style={[styles.item1, styles.item_1]}>
              <Text style={styles.title}>Albums</Text>
              <View style={styles.child}>
                <SafeAreaView>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={albums}
                    renderItem={({item}) => (
                      <TouchableOpacity onPress={() => goToAlbumsPage(item)}>
                        <View style={styles.children}>
                          <Image
                            style={styles.album_image}
                            source={{uri: item.url}}
                          />
                          <Text
                            style={[styles.albumName, styles.mt10]}
                            ellipsizeMode={'tail'}
                            numberOfLines={1}>
                            {item.name}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item) => item.name}
                  />
                </SafeAreaView>
              </View>
            </View>
            <View style={styles.item}>
              <Text style={styles.title}>Ca sĩ </Text>
              <View style={styles.child}>
                <SafeAreaView>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={singers}
                    renderItem={({item}) => (
                      <TouchableOpacity onPress={() => goSingerPage(item)}>
                        <View style={styles.children}>
                          <Image
                            style={styles.singer_image}
                            source={{uri: item.url}}
                          />
                          <Text
                            style={[styles.albumName, styles.mt5]}
                            ellipsizeMode={'tail'}
                            numberOfLines={1}>
                            {item.name}
                          </Text>
                          <View style={styles.singer_text}>
                            <View style={styles.fRow}>
                              <SimpleLineIcons
                                name={'user-follow'}
                                size={15}
                                color={'#666666'}
                              />
                              <Text style={styles.ml5Color}>
                                {kFormatter(item.hearts)}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item) => item.name}
                  />
                </SafeAreaView>
              </View>
            </View>
            <View style={styles.item2}>
              <Text style={styles.title}>Bài hát</Text>
              <View style={styles.child}>
                <SafeAreaView>
                  <FlatList
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    data={songs}
                    renderItem={({item}) => (
                      <TouchableOpacity onPress={() => goPlayPage(item)}>
                        <View style={styles.children}>
                          <Image
                            style={styles.song_image}
                            source={{uri: item.artwork}}
                          />
                          <Text
                            style={[styles.albumName, styles.mt5]}
                            ellipsizeMode={'tail'}
                            numberOfLines={1}>
                            {item.title}
                          </Text>
                          <Text style={styles.singer}>{item.artist}</Text>
                          <View style={styles.mt10fRow}>
                            <View style={styles.fRow}>
                              <Icon
                                name={'headphones'}
                                size={15}
                                color={'#666666'}
                              />
                              <Text style={styles.ml5Color}>
                                {kFormatter(item.plays)}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </TouchableOpacity>
                    )}
                    keyExtractor={(item) => item.name}
                  />
                </SafeAreaView>
              </View>
            </View>
          </ScrollView>
          {/*<CurrentPlayer />*/}
        </SafeAreaView>
      </View>
    );
  }
}

//style for component
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e6e6e6',
    flex: 1,
  },
  item: {
    height: 260,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  item1: {
    height: 240,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  item_1: {
    marginTop: 10,
  },
  item2: {
    height: 270,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  title: {
    fontSize: 30,
    paddingLeft: 10,
    color: 'black',
    marginTop: 10,
  },
  description: {
    marginLeft: 10,
    fontSize: 17,
    color: '#666666',
  },
  child: {
    marginTop: 10,
  },
  children: {
    marginLeft: 10,
    height: 180,
    width: 120,
    marginRight: 10,
  },
  albumName: {
    fontSize: 18,
    color: '#666666',
  },
  album_image: {
    width: 120,
    height: 120,
  },
  singer: {
    fontSize: 11,
    color: '#666666',
  },
  singer_image: {
    width: 120,
    height: 120,
  },
  header: {
    backgroundColor: 'white',
    marginBottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 15,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  mt10: {
    marginTop: 10,
  },
  mt5: {
    marginTop: 5,
  },
  singer_text: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
  },
  fRow: {
    flexDirection: 'row',
  },
  ml5Color: {
    marginLeft: 5,
    color: '#666666',
  },
  song_image: {width: 120, height: 120},
  mt10fRow: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 10,
  },
  isPlaying: {
    padding: 10,
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  progress: {
    height: 3,
    width: '100%',
    flexDirection: 'row',
  },
});
