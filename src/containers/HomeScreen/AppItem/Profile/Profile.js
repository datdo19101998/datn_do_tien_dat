import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  FlatList,
} from 'react-native';

import * as AuthAction from '../../../../redux/auth/actions';

import {useSelector, useDispatch} from 'react-redux';

import {ListItem} from 'react-native-elements';

import auth from '@react-native-firebase/auth';

import Icon from 'react-native-vector-icons/Feather';

import {isEmpty} from 'lodash';

export default function Profile({navigation}) {
  const dispatch = useDispatch();

  const [loading, setLoading] = useState(true);

  const {user, recent} = useSelector((state) => state.Auth);

  useEffect(() => {
    dispatch(AuthAction.getCurrentUser());
    if (!isEmpty(user)) {
      dispatch(AuthAction.getRecent(user));
    }
    if (isEmpty(recent, user)) {
      setTimeout(() => {
        setLoading(false);
      }, 500);
    }
  }, [dispatch, recent, user]);

  const renderSong = (items) => {
    return (
      <FlatList
        data={recent}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('Player', {
                id: item.id,
                name: item.name,
                url: item.url.cover,
                singer: item.singger,
                play: item.url.play,
                likes: item.likes,
                plays: item.plays,
              })
            }>
            <ListItem
              key={item.index}
              leftAvatar={{source: {uri: item.url}}}
              title={item.name}
              titleStyle={styles.colorBlack}
              subtitle={item.singger}
              // bottomDivider
              style={styles.mt5_br10}
            />
          </TouchableOpacity>
        )}
        keyExtractor={(item) => item.id}
      />
    );
  };

  if (loading) {
    return (
      <View style={styles.loading}>
        <ActivityIndicator color={'green'} size={'large'} />
      </View>
    );
  }
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <ScrollView>
          <TouchableOpacity
            style={styles.fRow}
            onPress={() =>
              navigation.navigate('ProfileDetail', {
                uid: user.uid,
              })
            }>
            <View style={styles.avatarContainer}>
              {user.photoURL && (
                <Image source={{uri: user.photoURL}} style={styles.avatar} />
              )}
              <Text style={styles.textEmail}>
                {user.email || user.displayName}
              </Text>
            </View>
            <View style={styles.editIcon}>
              <Icon name={'edit'} size={20} />
            </View>
          </TouchableOpacity>

          <View style={styles.item}>
            <ListItem
              containerStyle={styles.list1}
              title={'Playlist'}
              titleStyle={styles.colorBlack}
              leftIcon={{name: 'headset', color: 'black'}}
              bottomDivider
              chevron
            />
            <ListItem
              containerStyle={styles.list2}
              title={'Albums'}
              titleStyle={styles.colorBlack}
              leftIcon={{name: 'theaters', color: 'black'}}
              bottomDivider
              chevron
            />
            <ListItem
              containerStyle={styles.list3}
              title={'Following'}
              titleStyle={styles.colorBlack}
              leftIcon={{name: 'people', color: 'black'}}
              bottomDivider
              chevron
            />
            <ListItem
              containerStyle={styles.bgWhite}
              title={'Stations'}
              titleStyle={styles.colorBlack}
              leftIcon={{name: 'track-changes', color: 'black'}}
              chevron
            />
          </View>
          <View style={styles.item1}>
            <Text style={styles.fs20ClBlack}>Recently played</Text>
          </View>
          <View style={styles.item2}>
            {isEmpty(recent) ? (
              <View style={styles.recent}>
                <Text style={styles.recentText}>You dont have a recents</Text>
              </View>
            ) : (
              renderSong(recent)
            )}
          </View>
          <View style={styles.logoutContainer}>
            <TouchableOpacity onPress={() => auth().signOut()}>
              <Text style={styles.logout}>Logout</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#e6e6e6',
    flex: 1,
    padding: 10,
  },
  item: {
    height: 220,
    backgroundColor: 'white',
  },
  item1: {
    marginTop: 8,
    backgroundColor: 'white',
    height: 50,
    justifyContent: 'center',
    paddingLeft: 20,
    marginBottom: 5,
  },
  title: {
    fontSize: 20,
    color: 'red',
  },
  item2: {
    height: 350,
    backgroundColor: 'white',
  },
  item3: {
    backgroundColor: 'white',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 20,
    marginTop: 5,
  },
  button: {
    backgroundColor: '#1976D2',
    margin: 20,
  },
  mt5_br10: {
    marginTop: 5,
    borderRadius: 10,
  },
  colorBlack: {
    color: 'black',
  },
  recent: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  recentText: {color: 'gray', fontSize: 25},
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  fRow: {
    flexDirection: 'row',
  },
  avatarContainer: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginBottom: 5,
    height: 60,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingLeft: 10,
  },
  avatar: {height: 40, width: 40, borderRadius: 25},
  textEmail: {
    color: 'black',
    fontSize: 20,
    marginLeft: 10,
    fontWeight: 'bold',
  },
  editIcon: {
    backgroundColor: 'white',
    marginBottom: 5,
    height: 60,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 20,
  },
  list1: {
    backgroundColor: 'white',
    borderColor: '#f2f2f2',
    borderBottomWidth: 5,
  },
  list2: {
    backgroundColor: 'white',
    borderColor: '#f2f2f2',
    borderBottomWidth: 5,
  },
  list3: {
    backgroundColor: 'white',
    borderColor: '#f2f2f2',
    borderBottomWidth: 5,
  },
  bgWhite: {
    backgroundColor: 'white',
  },
  fs20ClBlack: {
    fontSize: 20,
    color: 'black',
  },
  logoutContainer: {
    height: 50,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
  },
  logout: {fontSize: 20, fontWeight: '600', color: 'red'},
});
