import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  Image,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  ScrollView,
  ActivityIndicator,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import Icon1 from 'react-native-vector-icons/Ionicons';

import firestore from '@react-native-firebase/firestore';

export default function PFProfile({route, navigation}) {
  console.disableYellowBox = true;
  const {uid} = route.params;
  const [loading, setLoading] = useState(true);
  const [user, setUser] = useState({});

  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [gender, setGender] = useState('');
  const [adress, setAdress] = useState('');
  // const [avatar, setAvatar] = useState('');

  const onUpdate = () => {
    if ((name !== '', phone !== '', gender !== '', adress !== '')) {
      firestore()
        .collection('users')
        .doc(uid)
        .collection('infomation')
        .doc(uid)
        .update({
          displayName: name,
          phoneNumber: phone,
          gender,
          adress,
        })
        // eslint-disable-next-line no-alert
        .then(() => alert('Update complete'));
    }
  };

  useEffect(() => {
    firestore()
      .collection('users')
      .doc(uid)
      .collection('infomation')
      .get()
      .then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          setName(doc.data().displayName);
          setAdress(doc.data().adress);
          setGender(doc.data().gender);
          setPhone(doc.data().phoneNumber);
          setUser(doc.data());
          // console.log(doc.data());
        });
      })
      .then(() => {
        setLoading(false);
      });
  }, [uid]);
  if (loading) {
    return (
      <ImageBackground
        source={{
          uri:
            'https://i.pinimg.com/originals/fe/e5/ea/fee5eab30a698c169dc4fd5752359c2c.jpg',
        }}
        style={styles.imgBackground}>
        <View style={styles.avatarContainer}>
          <ActivityIndicator size={'large'} color={'white'} />
        </View>
      </ImageBackground>
    );
  }
  return (
    <ImageBackground
      source={{
        uri:
          'https://i.pinimg.com/originals/fe/e5/ea/fee5eab30a698c169dc4fd5752359c2c.jpg',
      }}
      style={styles.avatarContainer}>
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.goBack}>
            <Icon1 name={'md-arrow-round-back'} size={30} color={'white'} />
          </TouchableOpacity>
          <View style={styles.avatar}>
            {!user.photoURL ? (
              <TouchableOpacity style={styles.userUrl}>
                <Icon name={'camera'} size={70} />
              </TouchableOpacity>
            ) : (
              <TouchableOpacity style={styles.iconCamera}>
                <Image
                  source={{
                    uri: user.photoURL,
                  }}
                  style={styles.img}
                />
              </TouchableOpacity>
            )}
          </View>
          <View style={styles.p_20}>
            <View>
              <Text style={styles.fs20}>Email</Text>
              <TextInput
                style={styles.TextInput1}
                placeholderTextColor={'black'}
                placeholder={user.email}
                editable={false}
              />
            </View>
            <View style={styles.mt20}>
              <Text style={styles.fs20}>Name</Text>
              <TextInput
                style={styles.TextInput2}
                placeholder={name}
                placeholderTextColor={'black'}
                onChangeText={(text) => setName(text)}
              />
            </View>
            <View style={styles.mt20}>
              <Text style={styles.fs20}>Phone</Text>
              <TextInput
                style={styles.TextInput2}
                placeholder={phone}
                onChangeText={(text) => setPhone(text)}
                placeholderTextColor={'black'}
              />
            </View>
            <View style={styles.mt20fRow}>
              <View style={styles.fee5eab30a698c169dc4fd5752359c2c}>
                <Text style={styles.fs20}>Signin With</Text>
                <TextInput
                  style={styles.TextInput1}
                  placeholderTextColor={'black'}
                  placeholder={user.providerData[0].providerId}
                  editable={false}
                />
              </View>
              <View style={styles.f1_ml10}>
                <Text style={styles.fs20}>gender</Text>
                <TextInput
                  style={styles.TextInput2}
                  placeholder={gender}
                  onChangeText={(text) => setGender(text)}
                  placeholderTextColor={'black'}
                />
              </View>
            </View>
            <View style={styles.mt20}>
              <Text style={styles.fs20}>Adress</Text>
              <TextInput
                placeholder={adress}
                style={styles.TextInput2}
                // multiline={true}
                onChangeText={(text) => setAdress(text)}
                placeholderTextColor={'black'}
              />
            </View>
            <View style={styles.alignItems}>
              <TouchableOpacity onPress={onUpdate} style={styles.TextInput2}>
                <Text style={styles.updateButton}>Update</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </ImageBackground>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // backgroundColor: 'white',
  },
  datePickerBox: {
    borderRadius: 4,
    height: 50,
    justifyContent: 'center',
    backgroundColor: 'white',
    paddingLeft: 5,
  },

  datePickerText: {
    fontSize: 20,
    marginLeft: 5,
    borderWidth: 0,
    color: '#000',
  },
  imgBackground: {
    height: '100%',
    width: '100%',
  },
  avatarContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatar: {
    height: 150,
    justifyContent: 'center',
    alignItems: 'center',
  },
  goBack: {
    height: 60,
    width: 60,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  userUrl: {
    borderWidth: 5,
    borderColor: 'black',
    padding: 20,
    height: 150,
    width: 150,
    borderRadius: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconCamera: {
    height: 150,
    width: 150,
    borderRadius: 75,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    height: '100%',
    width: '100%',
    borderRadius: 100,
  },
  p_20: {
    padding: 20,
  },
  fs20: {
    fontSize: 20,
  },
  TextInput1: {
    height: 50,
    backgroundColor: '#cccccc',
    paddingLeft: 10,
    borderRadius: 5,
    fontSize: 20,
  },
  mt20: {
    marginTop: 20,
  },
  TextInput2: {
    height: 50,
    backgroundColor: 'white',
    paddingLeft: 10,
    borderRadius: 5,
    fontSize: 20,
  },
  updateButton: {
    fontSize: 30,
    color: '#1976D2',
    fontWeight: 'bold',
  },
  mt20fRow: {
    marginTop: 20,
    flexDirection: 'row',
  },
  f1: {
    flex: 1,
  },
  f1_ml10: {
    flex: 1,
    marginLeft: 10,
  },
  alignItems: {
    alignItems: 'center',
  },
});
