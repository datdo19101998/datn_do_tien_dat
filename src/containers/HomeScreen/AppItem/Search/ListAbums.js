import React from 'react';
import {Text, StyleSheet, ImageBackground} from 'react-native';

export default function ListAlbums(props) {
  const name = props.name;
  return (
    <ImageBackground
      style={styles.img}
      source={{
        uri: props.urlPath,
      }}>
      <Text style={styles.text}>{name}</Text>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  text: {
    fontSize: 20,
    color: 'white',
  },
  img: {
    width: 190,
    height: 120,
    margin: 5,
    borderRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    opacity: 0.7,
  },
});
