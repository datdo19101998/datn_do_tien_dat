import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

export default function Item(props) {
  const name = props.name;
  const url = props.url;
  const singer = props.singer;
  // const id = props.id;
  // const play = props.urlPlay;

  return (
    <View style={styles.container}>
      <View style={styles.left}>
        <Image source={{uri: url}} style={styles.img} />
      </View>
      <View style={styles.right}>
        <Text numberOfLines={1} ellipsizeMode="tail" style={styles.name}>
          {name}
        </Text>
        <Text style={styles.singer}>{singer}</Text>
      </View>
      <View style={styles.close}>
        <Icon
          // onPress={() => this.onDelete(`${this.props.id}`)}
          name="download"
          size={25}
          color="black"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 10,
  },
  left: {
    height: 50,
    width: '15%',
  },
  right: {
    height: 50,
    width: '75%',
    flexDirection: 'column',
    paddingLeft: 10,
  },
  close: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    width: '10%',
  },
  img: {
    height: 50,
    width: 50,
    borderRadius: 10,
  },
  name: {
    fontSize: 20,
    color: 'black',
  },
  singer: {
    fontSize: 17,
    color: 'black',
  },
});
