import React, {useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';

import {useSelector, useDispatch} from 'react-redux';

import {isEmpty} from 'lodash';

const Stack = createStackNavigator();

import Home from '../Home/Home';
import HomeAlbum from '../Home/HomeAlbum';

import HomeSinger from '../Home/HomeSinger';

function MyStack({route}) {
  const {name} = useSelector((state) => state.Album);

  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerTitle: () => (
            <View style={styles.headerTitle}>
              <Image
                source={require('./../../../../images/logo.png')}
                style={styles.img}
              />
              <Text style={styles.text}>Box Music</Text>
            </View>
          ),
        }}
      />
      <Stack.Screen
        options={{headerShown: false}}
        name="HomeSinger"
        component={HomeSinger}
      />
      <Stack.Screen
        options={{
          headerTitle: () => (
            <View>
              <Text styles={styles.text}>{name}</Text>
            </View>
          ),
        }}
        name="HomeAlbum"
        component={HomeAlbum}
      />
    </Stack.Navigator>
  );
}

export default function HomeStack() {
  return <MyStack />;
}

const styles = StyleSheet.create({
  headerTitle: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  img: {
    height: 30,
    width: 30,
  },
  text: {
    marginLeft: 5,
    fontSize: 25,
    fontWeight: 'bold',
    color: '#0073e6',
  },
});
