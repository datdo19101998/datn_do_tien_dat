import React from 'react';

//import properties UI of react native
import {View, Text, Image, StyleSheet} from 'react-native';

//import react-native-vector-icons
import AntDesign from 'react-native-vector-icons/AntDesign';

import {useTrackPlayerProgress} from 'react-native-track-player';

//import for redux
import {useSelector} from 'react-redux';

import {isEmpty} from 'lodash';

const CurrentPlayer = () => {
  const progress = useTrackPlayerProgress();

  //value get of redux
  const {song} = useSelector((state) => state.Songs);

  const {title, artist, artwork} = song;

  if (!isEmpty(song)) {
    return (
      <View style={styles.container}>
        <View style={styles.progress}>
          <View style={[{flex: progress.position}, styles.b_orange]} />
          <View
            style={[
              {flex: progress.duration - progress.position},
              styles.b_grey,
            ]}
          />
        </View>
        <View style={styles.isPlaying}>
          <Image source={{uri: artwork}} style={styles.avatar} />
          <View style={styles.content}>
            <Text
              numberOfLines={1}
              ellipsizeMode={'tail'}
              style={styles.albumName}>
              {title}
            </Text>
            <Text>{artist}</Text>
          </View>
          <View>
            <AntDesign name={'pausecircleo'} size={40} color={'gray'} />
          </View>
        </View>
      </View>
    );
  } else {
    return <View />;
  }
};

const styles = StyleSheet.create({
  container: {
    height: '10%',
    display: 'flex',
    flexDirection: 'column',
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 25,
  },
  content: {
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  b_grey: {
    backgroundColor: 'grey',
  },
  b_orange: {
    backgroundColor: 'orange',
  },
  isPlaying: {
    padding: 10,
    backgroundColor: 'white',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  progress: {
    height: 3,
    width: '100%',
    flexDirection: 'row',
  },
});

export default CurrentPlayer;
