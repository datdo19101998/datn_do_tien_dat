import React from 'react';
import App from './app';
import {Provider} from 'react-redux';
import configStore from './config/configStore';

const store = configStore();

const index = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

export default index;
