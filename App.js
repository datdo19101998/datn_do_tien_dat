import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LandingScreen from './react/screens/LandingScreen';
import PlaylistScreen from './react/screens/PlaylistScreen';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={'Play'}>
        <Stack.Screen name="Home" component={LandingScreen} />
        <Stack.Screen name="Play" component={PlaylistScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;

// /**
//  * Sample React Native App
//  * https://github.com/facebook/react-native
//  *
//  * @format
//  * @flow strict-local
//  */

// import React, {useEffect} from 'react';
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';

// import TrackPlayer from 'react-native-track-player';

// const App: () => React$Node = () => {
//   console.disableYellowBox = true;
//   useEffect(() => {
//     // Creates the player
//     TrackPlayer.setupPlayer().then(async () => {
//       // Adds a track to the queue
//       await TrackPlayer.add({
//         id: 'trackId',
//         url:
//           'https://vnno-zn-5-tf-mp3-s1-zmp3.zadn.vn/d7ca79eaa3ae4af013bf/9002813141512780655?authen=exp=1590717379~acl=/d7ca79eaa3ae4af013bf/*~hmac=af3f403b4dff584bceeb4e0729e07c40&filename=Lac-Troi-Triple-D-Remix-Son-Tung-M-TP.mp3',
//         title: 'Track Title',
//         artist: 'Track Artist',
//         artwork:
//           'https://www.google.com/imgres?imgurl=https%3A%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fvi%2F5%2F5d%2FLac_troi_single_sontungmtp.jpg&imgrefurl=https%3A%2F%2Fvi.wikipedia.org%2Fwiki%2FL%25E1%25BA%25A1c_tr%25C3%25B4i&tbnid=vxtxCl1woPkymM&vet=12ahUKEwj917307ozpAhXIApQKHVKrDUUQMygBegUIARDpAQ..i&docid=SErex005WbCDBM&w=600&h=600&q=lac%20troi&ved=2ahUKEwj917307ozpAhXIApQKHVKrDUUQMygBegUIARDpAQ',
//       });
//       TrackPlayer.updateOptions({
//         capabilities: [
//           TrackPlayer.CAPABILITY_PLAY,
//           TrackPlayer.CAPABILITY_PAUSE,
//           TrackPlayer.CAPABILITY_STOP,
//         ],
//         compactCapabilities: [
//           TrackPlayer.CAPABILITY_PLAY,
//           TrackPlayer.CAPABILITY_PAUSE,
//           TrackPlayer.CAPABILITY_STOP,
//         ],
//       });

//       // Starts playing it
//       TrackPlayer.play();
//     });
//   }, []);

//   return (
//     <>
//       <StatusBar barStyle="dark-content" />
//       <SafeAreaView>
//         <ScrollView
//           contentInsetAdjustmentBehavior="automatic"
//           style={styles.scrollView}>
//           <Header />
//           {global.HermesInternal == null ? null : (
//             <View style={styles.engine}>
//               <Text style={styles.footer}>Engine: Hermes</Text>
//             </View>
//           )}
//           <View style={styles.body}>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Step One</Text>
//               <Text style={styles.sectionDescription}>
//                 Edit <Text style={styles.highlight}>App.js</Text> to change this
//                 screen and then come back to see your edits.
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>See Your Changes</Text>
//               <Text style={styles.sectionDescription}>
//                 <ReloadInstructions />
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Debug</Text>
//               <Text style={styles.sectionDescription}>
//                 <DebugInstructions />
//               </Text>
//             </View>
//             <View style={styles.sectionContainer}>
//               <Text style={styles.sectionTitle}>Learn More</Text>
//               <Text style={styles.sectionDescription}>
//                 Read the docs to discover what to do next:
//               </Text>
//             </View>
//             <LearnMoreLinks />
//           </View>
//         </ScrollView>
//       </SafeAreaView>
//     </>
//   );
// };

// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },
//   engine: {
//     position: 'absolute',
//     right: 0,
//   },
//   body: {
//     backgroundColor: Colors.white,
//   },
//   sectionContainer: {
//     marginTop: 32,
//     paddingHorizontal: 24,
//   },
//   sectionTitle: {
//     fontSize: 24,
//     fontWeight: '600',
//     color: Colors.black,
//   },
//   sectionDescription: {
//     marginTop: 8,
//     fontSize: 18,
//     fontWeight: '400',
//     color: Colors.dark,
//   },
//   highlight: {
//     fontWeight: '700',
//   },
//   footer: {
//     color: Colors.dark,
//     fontSize: 12,
//     fontWeight: '600',
//     padding: 4,
//     paddingRight: 12,
//     textAlign: 'right',
//   },
// });

// export default App;
